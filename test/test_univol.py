# PROJET SMITH 2020
# File test NRTL model
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import numpy as np
import numpy.testing as npt

from smith.ternary import univol
from smith.ternary import tools as tool
from smith import tools
from modelDatabase.acetone_chloroforme_methanol_nrtl_antoine import *
from modelDatabase.methylacetate_methanol_hexane_nrtl_antoine import *
from modelDatabase.acetone_chloroforme_benzene_nrtl_antoine import *
from modelDatabase.acetone_ethylacetate_benzene_nrtl_antoine import *
from modelDatabase.acetone_methanol_propanol_uniquac_dippr import *

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Ici  on test toutes les fonctions codés dans le module univol.py
# afin de voir si les testes passent. Pour cela nous avons utilisés le modèle
# acetone_chloroforme_methanol_nrtl.

 # On teste si la fonction phi est bien codé
def test_phi():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    edge = 1
    alpha = 0.653609
    x = np.array([53.4064])
    npt.assert_array_almost_equal(univol.phi(x, pressure, activity, edge, alpha), [0.0])

 # On teste si la fonction psi est bien codé
def test_psi():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    alpha = 0.653609
    edge = 1
    ind_i    = 2
    ind_j    = 3
    x = np.array([53.4064])
    npt.assert_array_almost_equal(univol.psi(x, pressure, activity, edge, alpha, ind_i, ind_j), [0.0])

 # On teste si la fonction extremity est bien codé
def test_extremity():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    edge = 1
    ind_i    = 2
    ind_j    = 3
    x = np.array([0.653609, 53.4064])
    npt.assert_array_almost_equal(univol.extremity(x, pressure, activity, edge, ind_i, ind_j), [0.0, 0.0])

# On teste si la jacobienne de la fonction phi est bien codé en la comparant à celle obtenue
# par différence finie
def test_phi_jac_diff_and_tapenade():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    jac_phi_diff = lambda T: tools.finite_diff(lambda y : univol.phi(y, pressure, activity, edge, alpha), T)
    jac_phi_tape = lambda T: univol.phi_jac(T, pressure, activity, edge, alpha)

    edge = 1
    alpha = 0.653609
    x = np.array([53.4064])

    print("Phi tapenade est : ", jac_phi_tape(x))
    print("Phi diff est : ", jac_phi_diff(x))

    npt.assert_array_almost_equal(jac_phi_tape(x), jac_phi_diff(x))

# On teste si la jacobienne de la fonction psi est bien codé en la comparant à celle obtenue
# par différence finie
def test_psi_jac_diff_and_tapenade():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    jac_psi_diff = lambda T: tools.finite_diff(lambda y : univol.psi(y, pressure, activity, edge, alpha, indi, indj), T)
    jac_psi_tape = lambda T: univol.psi_jac(T, pressure, activity, edge, alpha, indi, indj)

    edge = 1
    alpha = 0.653609
    indi    = 2
    indj    = 3
    x = np.array([53.4064])

    print("Psi tapenade est : ", jac_psi_tape(x))
    print("Psi diff est : ", jac_psi_diff(x))

    npt.assert_array_almost_equal(jac_psi_tape(x), jac_psi_diff(x))

# On teste si la jacobienne de la fonction extremity est bien codé en la comparant à celle obtenue
# par différence finie
def test_extremity_jac_diff_and_tapenade():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    jac_extremity_diff = lambda x: tools.finite_diff(lambda y : univol.extremity(y, pressure, activity, edge, indi, indj), x)
    jac_extremity_tape = lambda x: univol.extremity_jac(x, pressure, activity, edge, indi, indj)

    edge = 1
    indi    = 2
    indj    = 3
    x = np.array([0.653609, 53.4064])

    print("extremity tapenade est : ", jac_extremity_tape(x))
    print("extremity diff est : ", jac_extremity_diff(x))

    npt.assert_array_almost_equal(jac_extremity_tape(x), jac_extremity_diff(x))

# On test la fonction extremities sous différents edges
def test_extremities_edge1():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 1
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.653609, 53.40642]])

def test_extremities_edge2():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 2
    indice_i    = 1
    indice_j    = 3
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[1.0-0.792835, 55.366996]])

def test_extremities_edge3():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 3
    indice_i    = 1
    indice_j    = 2
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.351142, 65.107598]])

# On teste la jacobienne de la fonction curve
def test_fcurve_jac():
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    (pars_p, pars_g, modelName_p, modelName_g) = tool.set_pars_py_to_fortran(pressure, activity)
    x = np.array([0.653609, 53.4064])
    beta = 0.2
    indi = 1
    indj = 2
    print(univol.jac_fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj))

# On teste si la jacobienne de la fonction curve est identique à celle obtenue par différence finie
def test_fcurve_jac_diff_and_tapenade():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    (pars_p, pars_g, modelName_p, modelName_g) = tool.set_pars_py_to_fortran(pressure, activity)

    # On récupère la dérivée de la fonction x dfdx donnée par différence finie
    dfdx_fcurve_diff = lambda x, beta: tools.finite_diff(lambda y : univol.fcurve(y, beta, modelName_p, pars_p,
                                                                                  modelName_g, pars_g, indi, indj), x)

    # On récupère la dérivée de la fonction beta dfdbeta par différence finie
    fun = lambda x, beta : univol.fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj)
    dfdb_fcurve_diff = lambda x, beta : (fun(x,beta+1e-8)-fun(x, beta))/1e-8

    # On récupère la dérivée de la fonction df(x,beta) --> (dfdx, dfdbeta) donnée par tapenade
    jac_fcurve_tape = lambda x, beta: univol.jac_fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj)

    beta = np.array([0.2])
    indi    = 2
    indj    = 3
    x = np.array([0.653609, 53.4064])

    (dfdx, dfdb) = jac_fcurve_tape(x,beta)
    print("fcurvedx diff est : ", dfdx_fcurve_diff(x, beta))
    print("fcurvedx tapenade est : ", dfdx)

    print("fcurvedb diff est : ", dfdb_fcurve_diff(x, beta))
    print("fcurvedb tapenade est : ", dfdb[:,0])

    # On compare si les dérivées obtenues par différence finie et ceux obtenues avec tapenade sont identiques
    npt.assert_array_almost_equal(dfdx, dfdx_fcurve_diff(x, beta))
    npt.assert_array_almost_equal(dfdb[:,0], dfdb_fcurve_diff(x, beta))


# On teste les projections orthogonales des points du triangle
def test_proj():
    x1 =  0.5
    x2 = -0.01
    (x1, x2) = univol.proj(x1, x2)
    assert(x1 == 0.5)
    assert(x2 == 0.0)

    x1 = -0.01
    x2 = 0.5
    (x1, x2) = univol.proj(x1, x2)
    assert(x2 == 0.5)
    assert(x1 == 0.0)

    x3 = -0.01
    x1 = 0.6
    x2 = 1.0-x1
    a  = 0.05
    x1 = x1 + a
    x2 = x2 + a
    (x1, x2) = univol.proj(x1, x2)
    assert(x1 == 0.6)
    assert(x2 == 0.4)


def test_curve():

    # get extremities
    Temp_min    = 0
    Temp_max    = 100
    edge        = 1
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    extremities = univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min, Temp_max)

    extremity   = extremities[0]
    x1_init     = 0.0
    x2_init     = extremity[0]
    temperature_init = extremity[1]

    # On récupère les valeurs du calcul d'une courbe
    (x1, x2, T) = univol.curve(pressure, activity, x1_init, x2_init, temperature_init, indice_i, indice_j)

    print(x1[-1], x2[-1], T[-1])

    # on teste le dernier point
    edge = 2
    extremities = univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min, Temp_max)

    extremity_end   = extremities[0]
    x2_end     = 0.0
    x1_end     = 1.0 - extremity_end[0]
    temperature_end = extremity_end[1]

    # on teste si le dernier point se trouve sur une autre face du triangle
    npt.assert_almost_equal(x1_end, x1[-1])
    npt.assert_almost_equal(x2_end, x2[-1])
    npt.assert_almost_equal(temperature_end, T[-1])


def test_diagram():
    (pressure, activity)  = acetone_chloroforme_methanol_nrtl_antoine()
    curves_list = univol.diagram(pressure, activity, options=univol.Options(Display='off'))

    # On vérifie le nombre de courbe obtenues
    print(len(curves_list))

    # ajouter un test

# On a changé de modèle ici afin de voir si les tests passent pour cela nous avons utilisés le modèle
# methylacetate_methanol_hexane_nrtl et on a repris tous les test déja effectué avec quelques modifications

def test_phi_methylacetate():
    # On teste si la fonction phi est bien codé
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    edge = 1
    alpha = 0.271193
    x = np.array([49.7641])
    npt.assert_array_almost_equal(univol.phi(x, pressure, activity, edge, alpha), [0.0])


def test_psi_methylacetate():
    # On teste si la fonction psi est bien codé
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    edge = 1
    ind_i    = 1
    ind_j    = 2
    alpha = 0.271192
    x = np.array([49.764105])
    npt.assert_array_almost_equal(univol.psi(x, pressure, activity, edge, alpha, ind_i, ind_j), [-7e-6])


def test_extremity_methylacetate():
    # On teste si la fonction extremity est bien codé
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    edge = 1
    ind_i    = 1
    ind_j    = 2
    x = np.array([0.271192, 49.764105])
    npt.assert_array_almost_equal(univol.extremity(x, pressure, activity, edge, ind_i, ind_j), [0.0, -7e-6])

# On teste si la jacobienne de la fonction phi est bien codé en la comparant à celle obtenue
# par différence finie
def test_phi_jac_diff_and_tapenade_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    jac_phi_diff = lambda T: tools.finite_diff(lambda y : univol.phi(y, pressure, activity, edge, alpha), T)
    jac_phi_tape = lambda T: univol.phi_jac(T, pressure, activity, edge, alpha)

    edge = 1
    alpha = 0.271193
    x = np.array([49.7641])

    print("Phi tapenade est : ", jac_phi_tape(x))
    print("Phi diff est : ", jac_phi_diff(x))

    npt.assert_array_almost_equal(jac_phi_tape(x), jac_phi_diff(x))

# On teste si la jacobienne de la fonction psi est bien codé en la comparant à celle obtenue
# par différence finie
def test_psi_jac_diff_and_tapenade_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    jac_psi_diff = lambda T: tools.finite_diff(lambda y : univol.psi(y, pressure, activity, edge, alpha, indi, indj), T)
    jac_psi_tape = lambda T: univol.psi_jac(T, pressure, activity, edge, alpha, indi, indj)

    edge = 1
    indi    = 1
    indj    = 2
    alpha = 0.271193
    x = np.array([49.7641])

    print("Psi tapenade est : ", jac_psi_tape(x))
    print("Psi diff est : ", jac_psi_diff(x))

    npt.assert_array_almost_equal(jac_psi_tape(x), jac_psi_diff(x))

# On teste si la jacobienne de la fonction extremity est bien codé en la comparant à celle obtenue
# par différence finie
def test_extremity_jac_diff_and_tapenade_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    jac_extremity_diff = lambda x: tools.finite_diff(lambda y : univol.extremity(y, pressure, activity, edge, indi, indj), x)
    jac_extremity_tape = lambda x: univol.extremity_jac(x, pressure, activity, edge, indi, indj)

    edge = 1
    indi    = 1
    indj    = 2
    x = np.array([0.271193, 49.7641])

    print("extremity tapenade est : ", jac_extremity_tape(x))
    print("extremity diff est : ", jac_extremity_diff(x))

    npt.assert_array_almost_equal(jac_extremity_tape(x), jac_extremity_diff(x))

# On test la fonction extremities sous différents edges

def test_extremities_edge1_methylacetate():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 1
    indice_i    = 1
    indice_j    = 3
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.711219, 49.759482]])

def test_extremities_edge2_methylacetate():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 2
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[1-0.90021, 53.06039]])

def test_extremities_edge3_methylacetate():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 3
    indice_i    = 1
    indice_j    = 2
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.652513, 53.052975]])

# On teste la jacobienne de la fonction curve
def test_fcurve_jac_methylacetate():
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    (pars_p, pars_g, modelName_p, modelName_g) = tool.set_pars_py_to_fortran(pressure, activity)
    x = np.array([0.652513, 53.053])
    beta = 0.2
    indi = 1
    indj = 2
    print(univol.jac_fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj))

# On teste si la jacobienne de la fonction curve est identique à celle obtenue par différence finie
def test_fcurve_jac_diff_and_tapenade_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    (pars_p, pars_g, modelName_p, modelName_g) = tool.set_pars_py_to_fortran(pressure, activity)

    # On calcul la dérivée de la fonction dfdx par différence finie
    dfdx_fcurve_diff = lambda x, beta: tools.finite_diff(lambda y : univol.fcurve(y, beta, modelName_p, pars_p, 
                                                                                  modelName_g, pars_g, indi, indj), x)

    # On récupère la dérivée de la fonction beta dfdbeta par différence finie
    fun = lambda x, beta : univol.fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj)
    dfdb_fcurve_diff = lambda x, beta : (fun(x,beta+1e-8)-fun(x, beta))/1e-8

    # On récupère la dérivée de la fonction df(x,beta) --> (dfdx, dfdbeta) donnée par tapenade
    jac_fcurve_tape = lambda x, beta: univol.jac_fcurve(x, beta, modelName_p, pars_p, modelName_g, pars_g, indi, indj)

    beta = np.array([0.2])
    indi    = 2
    indj    = 3
    x = np.array([0.652513, 53.053])

    (dfdx, dfdb) = jac_fcurve_tape(x,beta)
    print("fcurvedx diff est : ", dfdx_fcurve_diff(x, beta))
    print("fcurvedx tapenade est : ", dfdx)

    print("fcurvedb diff est : ", dfdb_fcurve_diff(x, beta))
    print("fcurvedb tapenade est : ", dfdb[:,0])

    # On compare si les dérivées obtenues par différence finie et ceux obtenues avec tapenade sont identiques
    npt.assert_array_almost_equal(dfdx, dfdx_fcurve_diff(x, beta))
    npt.assert_array_almost_equal(dfdb[:,0], dfdb_fcurve_diff(x, beta))


def test_curve_methylacetate():

    # On récupère les extremités
    Temp_min    = 0
    Temp_max    = 100
    edge        = 1
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    extremities = univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min, Temp_max)

    extremity   = extremities[0]
    x1_init     = 0.0
    x2_init     = extremity[0]
    temperature_init = extremity[1]

    # On calcul une courbe
    (x1, x2, T) = univol.curve(pressure, activity, x1_init, x2_init, temperature_init, indice_i, indice_j)

    print(x1[-1], x2[-1], T[-1])

    # on teste si le dernier point se trouve sur une autre face du triangle
    edge = 2
    extremities = univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min, Temp_max)

    extremity_end   = extremities[0]
    x2_end     = 0.0
    x1_end     = 1.0 - extremity_end[0]
    temperature_end = extremity_end[1]

    npt.assert_almost_equal(x1_end, x1[-1])
    npt.assert_almost_equal(x2_end, x2[-1])
    npt.assert_almost_equal(temperature_end, T[-1])


def test_diagram_methylacetate():
    (pressure, activity)  = methylacetate_methanol_hexane_nrtl_antoine()
    curves_list = univol.diagram(pressure, activity)

    # On vérifie si l'on obtient le nombre de courbe nécessaire qui est de 3 dans ce cas
    print(len(curves_list))

def test_extremities_edge1_acebenze():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 1
    indice_i    = 1
    indice_j    = 2
    (pressure, activity)  = acetone_chloroforme_benzene_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.571479, 69.665231]])


def test_extremities_edge2_acebenze():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 2
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = acetone_chloroforme_benzene_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[1-0.445977, 63.734762]])

def test_extremities_edge3_acebenze():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 3
    indice_i    = 1
    indice_j    = 2
    (pressure, activity)  = acetone_chloroforme_benzene_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.351142, 65.107598]])


def test_diagram_chloroforme_benzene():
    (pressure, activity)  = acetone_chloroforme_benzene_nrtl_antoine()
    curves_list = univol.diagram(pressure, activity)

    # On vérifie si l'on obtient le nombre de courbe nécessaire qui est de 3 dans ce cas
    print(len(curves_list))


def test_extremities_edge2_ethylacebenze():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 2
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = acetone_ethylacetate_benzene_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[1-0.277207, 67.234189]])

def test_extremities_edge3_ethylacebenze():
    Temp_min    = 0
    Temp_max    = 100
    edge        = 3
    indice_i    = 2
    indice_j    = 3
    (pressure, activity)  = acetone_ethylacetate_benzene_nrtl_antoine()
    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
                                                    Temp_max), [[0.154881, 71.897868]])


def test_diagram_ethylacebenze():
    (pressure, activity)  = acetone_ethylacetate_benzene_nrtl_antoine()
    curves_list = univol.diagram(pressure, activity)

    # On vérifie si l'on obtient le nombre de courbe nécessaire qui est de 3 dans ce cas
    print(len(curves_list))

#def test_phi_acemepro():
#    (pressure, activity) = acetone_methanol_propanol_uniquac_dippr()
#    edge = 1
#    alpha = 0.2226
#    x = np.array([328.392721])
#    npt.assert_array_almost_equal(univol.phi(x, pressure, activity, edge, alpha), [0.0])
#    
#    
#def test_extremities_edge1_uniquac_acemepro():
#    Temp_min    = 0
#    Temp_max    = 100
#    edge        = 1
#    indice_i    = 1
#    indice_j    = 2
#    (pressure, activity)  = acetone_methanol_propanol_uniquac_dippr()
#    npt.assert_array_almost_equal(univol.extremities(pressure, activity, indice_i, indice_j, edge, Temp_min,
#                                                    Temp_max), [[0.222600, 328.392721]])