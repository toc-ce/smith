import numpy as np
import smith

#-------------------------------------------------------------------------------------------
#    Description
#        Computes all the model's parameters (Nrtl) of the 3_components mixture
#                   (Methyl Acetate-Methanol-Hexane)     
#-------------------------------------------------------------------------------------------
#    Python Usage  
#        parameteres = acetate_methanol_hexane_nrtl()

#    Entrées
#        aucune 
 
#    Sorties
#        parameters : struct, contient tous les paramètres du mélange
#-------------------------------------------------------------------------------------------
def acetone_methanol_propanol_uniquac_dippr() :
    
   # La matrice A_bar du mélange :
   A = [[0, 447.425, -302.839], [-104.866, 0, 136.246], [519.69, -57.5886, 0]]
   r = [2.5735, 1.4311, 2.7799]
   q = [2.336, 1.432, 2.5120]
   Qp = q
   z = 10

   # Les coefficients dippr du mélange acetone :
   dippr_acetone = [69.006, -5599.6, -7.0985, 6.2237*1e-6, 2]

   # Les coefficients dippr du mélange de Methanol :
   dippr_methanol = [82.718, -6904.5, -8.8622, 7.4664*1e-6, 2]

   # Les coefficients dippr du mélange propanol :
   dippr_propanol = [94.126, -8604.8, -10.11, 3.1334*1e-6, 2]
   
   P0 = 101325

   # La structure des paramètres pour antoine et activity :
   pressure = smith.ternary.pressure.dippr(dippr_acetone, dippr_methanol, dippr_propanol, P0)
   activity = smith.ternary.activity.uniquac(A, r, q, Qp, z)
   

   return (pressure, activity)