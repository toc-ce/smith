import numpy as np
import smith

#-------------------------------------------------------------------------------------------
#    Description
#        Computes all the model's parameters (Nrtl) of the 3_components mixture
#                   (Methyl Acetate-Methanol-Hexane)     
#-------------------------------------------------------------------------------------------
#    Python Usage  
#        parameteres = acetate_methanol_hexane_nrtl()

#    Entrées
#        aucune 
 
#    Sorties
#        parameters : struct, contient tous les paramètres du mélange
#-------------------------------------------------------------------------------------------
def diethylamine_chloroforme_methanol_nrtl_dippr() :
    
   # La matrice A_bar du mélange :
   A = [[0, 199.5, -1138.17], [-939.9, 0, 1371.3], [682.134, -142.885, 0]]
   alpha = [[0, 0.2, 0.3], [0.2, 0, 0.3], [0.3, 0.3, 0]]

   # Les coefficients dippr du mélange diethylamine :
   dippr_diethylamine = [49.314, -4949, -3.9256, 9.1978*1e-18, 6]

   # Les coefficients dippr du mélange de chloroforme :
   dippr_chloroforme = [146.43, -7792.3, -20.614, 0.024578, 1]

   # Les coefficients dippr du mélange methanol :
   dippr_methanol = [82.718, -6904.5, -8.8622, 7.4664*1e-6, 2] 
   
#   P0 = 182133.57260915186
#   P0 = 0.92101325*101325
   P0=0.6279630271165363*101325

   # La structure des paramètres pour antoine et activity :
   pressure = smith.ternary.pressure.dippr(dippr_diethylamine, dippr_chloroforme, dippr_methanol, P0)
   activity = smith.ternary.activity.nrtl(A, alpha)
   

   return (pressure, activity)