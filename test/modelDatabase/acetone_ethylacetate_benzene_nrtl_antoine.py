
import numpy as np
import smith

#-------------------------------------------------------------------------------------------
#    Description
#        Computes all the model's parameters (Nrtl) of the 3_components mixture
#                   (Methyl Acetate-Methanol-Hexane)     
#-------------------------------------------------------------------------------------------
#    Python Usage  
#        parameteres = acetate_methanol_hexane_nrtl()

#    Entrées
#        aucune 
 
#    Sorties
#        parameters : struct, contient tous les paramètres du mélange
#-------------------------------------------------------------------------------------------
def acetone_ethylacetate_benzene_nrtl_antoine() :
   # La matrice A_bar du mélange :
   A_bar = np.array([[0, 529.7, -193.34], [-360, 0, -273.017], [569.931, 383.126, 0]])

   # La matrice alpha du mélange :
   alpha = np.array([[0, 0.2, 0.3007], [0.2, 0, 0.3194], [0.3007, 0.3194, 0]])

   # Les coefficients d'antoine de la premiere composantes du mélange :
   antoine_acetone = [7.11714, 1210.595, 229.664]

   # Les coefficients d'antoine de la deuxième composantes du mélange :
   antoine_ethylacetate = [7.10179, 1244.951, 217.881]

   # Les coefficients d'antoine de la troisièmes composante du mélange :
   antoine_benzene = [6.87987, 1196.760, 219.161]


   # La structure des paramètres pour antoine et activity :
   pressure = smith.ternary.pressure.antoine(antoine_acetone, antoine_ethylacetate, antoine_benzene)
   activity = smith.ternary.activity.nrtl(A_bar, alpha)
   

   return (pressure, activity)
