import numpy as np
import smith

def acetone_chloroforme_methanol_nrtl_antoine():
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONTRAT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#% The function return the parameters characterize the mixture (Acetone-Chloroforme-Methanol)
#% parametrers : No parameters
#% resultat : two structure named "pressure" and "activity" wich contains :
#%                     A_bar : the matrix (3*3) A_bar of binary interactions
#%                     alpha : the matrix (3*3) alpha of binary interactions
#%                    antoine : a structure with tree fields
#%                                      antoine 1 : the vector (1*3) of antoine's coefficients 1 [a1 b1 c1]
#%                                      antoine 2 : the vector (1*3) of antoine's coefficients 2 [a2 b2 c2]
#%                                      antoine 3 : the vector (1*3) of antoine's coefficients 3 [a3 b3 c3]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # The matrix A_bar of the mixture
    A_bar = np.array([[0, -643.277, 184.701], [228.457, 0, 2736.86], [222.645, -1244.03, 0]])

    # The matrix alpha of the mixture
    alpha = np.array([[0, 0.3043, 0.3084], [0.3043, 0, 0.095], [0.3084, 0.095, 0]])

    # The antoine coefs A of the mixture [a1 a2 a3]
    antoine_acetone = [7.11714, 1210.595, 229.664]

    # The antoine coefs B of the mixture [b1 b2 b3]
    antoine_chloroforme = [6.95465, 1170.966, 226.232]

    # The antoine coefs C of the mixture [c1 c2 c3]
    antoine_methanol = [8.08097, 1582.271, 239.726]

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # La structure des paramètres pour antoine et activity :
    pressure = smith.ternary.pressure.antoine(antoine_acetone, antoine_chloroforme, antoine_methanol) # P0 étant un paramètre optionnel à pressure
    activity = smith.ternary.activity.nrtl(A_bar, alpha) # R étant un paramètre optionnel à activity
   

    return (pressure, activity)
