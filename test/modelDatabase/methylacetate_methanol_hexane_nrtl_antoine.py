
import numpy as np
import smith

#-------------------------------------------------------------------------------------------
#    Description
#        Computes all the model's parameters (Nrtl) of the 3_components mixture
#                   (Methyl Acetate-Methanol-Hexane)     
#-------------------------------------------------------------------------------------------
#    Python Usage  
#        parameteres = acetate_methanol_hexane_nrtl()

#    Entrées
#        aucune 
 
#    Sorties
#        parameters : struct, contient tous les paramètres du mélange
#-------------------------------------------------------------------------------------------
def methylacetate_methanol_hexane_nrtl_antoine() :
    
   # La matrice A_bar du mélange :
   A_bar = np.array([[0, 441.452, 647.05], [304.005, 0, 1619.38], [403.459, 1622.29, 0]])

   # La matrice alpha du mélange :
   alpha = np.array([[0, 0.1174, 0.2], [0.1174, 0, 0.4365], [0.2, 0.4365, 0]])

   # Les coefficients d'antoine du mélange de Methyl Acetate :
   antoine_methylacetate = [7.06524, 1157.630, 219.726]

   # Les coefficients d'antoine du mélange de Methanol :
   antoine_methanol = [8.08097, 1582.271, 239.726]

   # Les coefficients d'antoine du mélange d'Hexane :
   antoine_hexane = [6.91058, 1189.640, 226.280] 

   # La structure des paramètres pour antoine et activity :
   pressure = smith.ternary.pressure.antoine(antoine_methylacetate, antoine_methanol, antoine_hexane)
   activity = smith.ternary.activity.nrtl(A_bar, alpha)
   

   return (pressure, activity)
