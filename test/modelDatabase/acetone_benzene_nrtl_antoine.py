"""
----------------------------------------------------------------------------------------
 
        Model's parameters (Nrtl) of the binary mixture  Acetone (1) - Benzene(2)
-------------------------------------------------------------------------------------------
    Author : N. Shcherbakova
    Date : 11/06/2020

    Inputs
        none

    Outputs

        parameters_bin : - struct, contains all the parameters of the binary mixture

    IMPORTANT

         the units used are :
                       R : the constant R [J/(K.mol)]
                       P0 : the external pressure [(mmHg)]
                       T : °C
-------------------------------------------------------------------------------------------
"""
import numpy as np
import smith

def acetone_benzene_nrtl_antoine():
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONTRAT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#% The function returns the parameters of the binary mixture Acetone (1) - Benzene (2)
#% IN : No parameters
#% OUT: a structure named "parametres" wich contains :
#%                     name : the name of the model (NRTL)
#%                     A_bar : the 2 x 2 matrix of binary interactions
#%                     alpha : symmetric 2 x 2 matrix   
#%                     antoine: structure with 2 fields :
#                               antoine1 : a (1*3) vector of Antoine's coefficients [a b c] of the first component
#                               antoine2 : a (1*3) vector of Antoine's coefficients [a b c] of the second component
#%                     R : the constant R [J/(K.mol)]
#%                     P0 : the external pressure [(mmHg)]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # coefficients d'interactions binaires (cal/mol)
    a12 = -193.34
    a21 = 569.931

    # non-randomness parameter
    alpha = 0.3007


    # a,b,c constant for Antoine's low written in the form P_sat= 10^(a1 - b1/(T + c1)) with T in °C

    Antoine_acetone = np.array([7.11714, 1210.595, 229.664]) 
    Antoine_benzene = np.array([6.87987, 1196.760, 219.161])
    

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    # La structure des paramètres pour antoine et activity :
    pressure = smith.binary.pressure.antoine(Antoine_acetone, Antoine_benzene)
    activity = smith.binary.activity.nrtl(a12, a21, alpha)
   

    return (pressure, activity)

