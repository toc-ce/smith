import numpy as np
import smith
#-------------------------------------------------------------------------------------------
#    Description
#        Calcul tous les paremètres (nrtl) des 3 composants du mélange    
#-------------------------------------------------------------------------------------------
#    Python Usage  
#        parameteres = acetone_chloroforme_benzene_nrtl()

#    Entrées
#        aucune 
# 
#    Sorties
#        parameters : struct, contient tous les paramètres du mélange
#-------------------------------------------------------------------------------------------

def acetone_chloroforme_benzene_nrtl_antoine() :

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   # La matrice A_bar du mélange :
   A_bar = np.array([[0, -643.277, -193.34], [228.457, 0, 176.8791], [569.931, -288.2136, 0]])

   # La matrice alpha du mélange :
   alpha = np.array([[0, 0.3043, 0.3007], [0.3043, 0, 0.3061], [0.3007, 0.3061, 0]])

   # Les coefficients d'antoine du mélange de Methyl Acetate :
   antoine_acetone = [7.11714, 1210.595, 229.664]

   # Les coefficients d'antoine du mélange de Methanol :
   antoine_chloroforme = [6.95465, 1170.966, 226.232]

   # Les coefficients d'antoine du mélange d'Hexane :
   antoine_benzene = [6.87087, 1196.760, 219.161]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   # La structure des paramètres pour antoine et activity :
   pressure = smith.ternary.pressure.antoine(antoine_acetone, antoine_chloroforme, antoine_benzene)
   activity = smith.ternary.activity.nrtl(A_bar, alpha)
   

   return (pressure, activity)
