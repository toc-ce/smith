import numpy as np
import smith

def acetone_chloroforme_nrtl_antoine():
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONTRAT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#% The function return the parameters characterize the mixture (Acetone-Chloroforme-Methanol)
#% parametrers : No parameters
#% resultat : two structure named "pressure" and "activity" wich contains :
#%                    a12, a21   : the float parameters of binary interactions
#%                     alpha    :   the coellicient alpha of binary interactions
#%                    antoine  : a structure with tree fields
#%                                      antoine 1 : the vector (1*3) of antoine's coefficients 1 [a1 b1 c1]
#%                                      antoine 2 : the vector (1*3) of antoine's coefficients 2 [a2 b2 c2]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE PARAMETERS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # binary activity coefficients
    a12=-643.277
    a21=228.457

    # alpha constant of the bianry mixture
    alpha=0.3043

    # a,b,c constant for Antoine's low written in the form P_sat= 10^(a1 - b1/(T + c1)) with T in °C

    Antoine_acetone = np.array([7.11714, 1210.595, 229.664]) 
    Antoine_chloroforme = np.array([6.95465, 1170.966, 226.232]) 

    #%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONSTRUCTION OF THE STRUCTURE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # La structure des paramètres pour antoine et activity :
    pressure = smith.binary.pressure.antoine(Antoine_acetone, Antoine_chloroforme)
    activity = smith.binary.activity.nrtl(a12, a21, alpha)

    return (pressure, activity)
