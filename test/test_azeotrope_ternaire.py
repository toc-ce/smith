# PROJET SMITH 2017
# File test NRTL model
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import numpy as np
import numpy.testing as npt

from smith.ternary import azeotrope
from smith import tools
from modelDatabase.acetone_chloroforme_methanol_nrtl_antoine import *
from modelDatabase.methylacetate_methanol_hexane_nrtl_antoine import *
from modelDatabase.acetone_methanol_propanol_uniquac_dippr import *
from modelDatabase.diethylamine_chloroforme_methanol_nrtl_dippr import *

# Test effectué pour le mélange acetone_chloroforme_methanol_nrtl ternaire et acetone_chloroforme_nrtl binaire
# et acetone_benzene_nrtl sans azéotrope.

# On teste la fonction azeotrope ternaire d'un mélange avec azéotrope
def test_F3():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    x = np.array([0.3675803876816134, 0.21068228840441117, 56.994317544478605])
    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0, 0.0])

# On teste résolution de la fonction azeotrope ternaire d'un mélange avec azéotrope
def test_solveF3():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    x0 = np.array([0.36, 0.21, 56.99])
    sol = azeotrope.solve(x0, pressure, activity)
    if sol.success is True:
        npt.assert_array_almost_equal(sol.x, [0.3675803876816134, 0.21068228840441117, 56.994318])
        assert sol.success is True


# On teste la jacobienne obtenue avec tapenade est équivalent à celle obtenue par différence finie
# pour les mélanges binaires et ternaires

def test_F3d_Tapenade_and_jac_diff_f3():
    (pressure, activity) = acetone_chloroforme_methanol_nrtl_antoine()
    jac_f3_diff = lambda x: tools.finite_diff(lambda y : azeotrope.fun(y, pressure, activity), x)
    jac_f3_tape = lambda x: azeotrope.jac(x, pressure, activity)

    x = np.array([0.3675803876816134, 0.21068228840441117, 56.994317544478605])
    npt.assert_array_almost_equal(jac_f3_tape(x), jac_f3_diff(x))

    x = np.array([0.1, 0.2, 20.0])
    npt.assert_array_almost_equal(jac_f3_tape(x), jac_f3_diff(x))


# On a changé de mélange (methylacetate_methanol_hexane_nrtl) et on a refait
# les memes testes que ceux effectués plus haut

def test_F3_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    x = np.array([0.330539, 0.317573, 47.368693])
    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0, 0.0])

def test_solveF3_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    x0 = np.array([0.33, 0.31, 47.36])
    sol = azeotrope.solve(x0, pressure, activity)
    if sol.success is True:
        npt.assert_array_almost_equal(sol.x, [0.330539, 0.317573, 47.368693])
        assert sol.success is True

def test_F3d_Tapenade_and_jac_diff_f3_methylacetate():
    (pressure, activity) = methylacetate_methanol_hexane_nrtl_antoine()
    jac_f3_diff = lambda x: tools.finite_diff(lambda y : azeotrope.fun(y, pressure, activity), x)
    jac_f3_tape = lambda x: azeotrope.jac(x, pressure, activity)

    x = np.array([0.330539, 0.317573, 47.368693])
    npt.assert_array_almost_equal(jac_f3_tape(x), jac_f3_diff(x))

    x = np.array([0.1, 0.2, 20.0])
    npt.assert_array_almost_equal(jac_f3_tape(x), jac_f3_diff(x))

def test_F3_uniquac_dippr():
    (pressure, activity) = acetone_methanol_propanol_uniquac_dippr()
    x = np.array([0.7773999309791952, 0.2226000690208047, 328.3927216465689])
    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0, 0.0])
    
def test_solveF3_uniquac_dippr():
    (pressure, activity) = acetone_methanol_propanol_uniquac_dippr()
    x0 = np.array([0.77, 0.22, 328.39])
    sol = azeotrope.solve(x0, pressure, activity)
    if sol.success is True:
        npt.assert_array_almost_equal(sol.x, [0.777400434, 0.222599566, 328.392728])
        assert sol.success is True
    
#def test_F3_nrtl_dippr():
#    (pressure, activity) = diethylamine_chloroforme_methanol_nrtl_dippr()
#    x = np.array([0.4693168730735793, 0.53068312692642, 68.43524021295511+273.15])
#    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0, 0.0])
    
#def test_F3_nrtl_dippr_complx():
#    (pressure, activity) = diethylamine_chloroforme_methanol_nrtl_dippr()
#    x = np.array([0.46085789682147943, 0.5391421031785232, 87.88430941216983+273.15])
#    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0, 0.0])