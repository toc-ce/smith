# PROJET SMITH 2017
# File test NRTL model
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

import numpy as np
import numpy.testing as npt

from smith.binary import azeotrope
from smith import tools
from modelDatabase.acetone_chloroforme_nrtl_antoine import *
from modelDatabase.acetone_benzene_nrtl_antoine import *

# Test effectué pour le mélange binaire et acetone_benzene_nrtl sans azéotrope.


# On teste la fonction azeotrope binaire d'un mélange avec azéotrope
def test_F2():
    (pressure, activity) = acetone_chloroforme_nrtl_antoine()
    x = np.array([0.351142, 65.1076]) #0.648858,
    npt.assert_array_almost_equal(azeotrope.fun(x, pressure, activity), [0.0, 0.0])

# On teste résolution de la fonction azeotrope binaire d'un mélange avec azéotrope
def test_solveF2():
    (pressure, activity) = acetone_chloroforme_nrtl_antoine()
    x0 = np.array([0.35, 65.10])
    sol = azeotrope.solve(x0, pressure, activity)
    if sol.success is True:
        npt.assert_array_almost_equal(sol.x, [0.351142, 65.107598])
        assert sol.success is True

# On teste la  résolution d'un mélange binaire sans azéotrope
def test_solveF2_without_azeotrope():
    (pressure, activity) = acetone_benzene_nrtl_antoine()
    x0 = np.array([0.45, 64.0])
    sol = azeotrope.solve(x0, pressure, activity)
    if sol.success is True:
        npt.assert_array_almost_equal(sol.x, [0.45, 64.0])
        assert sol.success is True


# On teste la jacobienne obtenue avec tapenade est équivalent à celle obtenue par différence finie
# pour les mélanges binaires et ternaires

def test_F2d_Tapenade_and_jac_diff_f2():
    (pressure, activity) = acetone_chloroforme_nrtl_antoine()
    jac_f2_diff = lambda x: tools.finite_diff(lambda y : azeotrope.fun(y, pressure, activity), x)
    jac_f2_tape = lambda x: azeotrope.jac(x, pressure, activity)

    x = np.array([0.351142, 65.107598])
    npt.assert_array_almost_equal(jac_f2_tape(x), jac_f2_diff(x))

    x = np.array([0.1, 20.0])
    npt.assert_array_almost_equal(jac_f2_tape(x), jac_f2_diff(x))
