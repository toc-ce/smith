# smith

smith (Separation of Mixtures In Thermo by Homotopy) is an open-source software designed to compute univolatility
diagrams of ternary mixtures in Thermodynamic.

[![Online examples](https://img.shields.io/badge/smith-documentation-blue.svg)](https://toc-ce.gitlab.io/smith/)
[![Full directory](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/toc-ce%2Fsmith/develop?urlpath=lab/)



