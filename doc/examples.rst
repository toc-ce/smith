Examples
========

.. toctree::
   :maxdepth: 1

   examples/azeotrope/azeotrope_bin
   examples/azeotrope/azeotrope_tern
   examples/univolatility/univol_acb
   examples/univolatility/univol_acm
   examples/univolatility/univol_aeb
   examples/univolatility/univol_mamh
