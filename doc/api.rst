API documentation
=================


.. toctree::
   :maxdepth: 2
   :caption: Binary

   api/activity_binary
   api/pressure_binary
   api/azeotrope_binary
   


.. toctree::
   :maxdepth: 2
   :caption: Ternary

   api/activity_ternary
   api/pressure_ternary
   api/azeotrope_ternary
   api/univol_ternary


