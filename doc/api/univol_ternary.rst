smith.ternary.univol
====================

.. automodule:: smith.ternary.univol
   :members: diagram, extremities, curve
