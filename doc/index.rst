Welcome to smith documentation
==============================

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   overview
   installation

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   api
   examples

Index
=====

* :ref:`genindex`

.. * :ref:`modindex`
.. * :ref:`search`
